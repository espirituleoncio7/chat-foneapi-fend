import { Avatar } from '@chakra-ui/avatar';
import { Box, Text } from '@chakra-ui/layout';
// import { useChatContext } from '../../contextAPI/chatProvider';

const UserListItem = ({ user, handleFunction }) => {
  //   const { user } = useChatContext();

  return (
    <Box
      onClick={handleFunction}
      style={{ transition: '0.3s ease', display: 'flex' }}
      cursor="pointer"
      bg="#E8E8E8"
      _hover={{
        background: '#1c1e27',
        color: 'white',
      }}
      w="100%"
      d="flex"
      alignItems="center"
      color="black"
      px={3}
      py={2}
      mb={2}
      borderRadius="lg"
    >
      <Avatar
        mr={2}
        size="sm"
        cursor="pointer"
        name={user.name}
        src={user.pic}
      />
      <Box>
        <Text>{user.name}</Text>
        <Text fontSize="xs">
          <b>Email : </b>
          {user.email}
        </Text>
      </Box>
    </Box>
  );
};

export default UserListItem;
